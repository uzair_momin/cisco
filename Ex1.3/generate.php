<?php

$dbconfig['db_host_name'] = 	'localhost';
$dbconfig['db_user_name'] = 	'root';
$dbconfig['db_password'] = 	'';
$dbconfig['db_name'] = 	'cisco';

$conn = mysqli_connect($dbconfig['db_host_name'], $dbconfig['db_user_name'], $dbconfig['db_password']);

if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}

mysqli_select_db($conn,$dbconfig['db_name']);

function generateRandomString($length = 10,$characters) {

	if($characters == '')
    	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function generate($no)
{
	global $conn;
	$data = [];
	$loobackArr = [];
	$hostnameArr = [];
	$type = 'CSS';
	for($i=0;$i<$no;$i++)
	{
		$sapId = rand(1111111111,9999999999);;
		$hostname = 'www.'.generateRandomString(6,'').'.com';
		$loopback = long2ip(rand(0, "4294967295"));
		$macaddress = implode(':', str_split(substr(md5(mt_rand()), 0, 12), 2));
		//echo $hostname." ".$loopback." ".$macaddress."<br>";

		while(true)
		{
			if(array_search($hostname,$hostnameArr) === false)
				break;
			else
				$hostname = 'www.'.generateRandomString(6,'').'.com';
		}
		while(true)
		{
			if(array_search($looback,$loobackArr) === false)
				break;
			else
				$loopback = long2ip(rand(0, "4294967295"));
			
		}

		$insert_q = "INSERT INTO router_prop (sap_id,hostname,loopback,mac_addr,type) Values ('".$sapId."','".$hostname."',INET_ATON('".$loopback."'),'".$macaddress."','".$type."')";
		
		$type = ($type == 'CSS') ? 'AG1' : 'CSS';
		mysqli_query($conn,$insert_q);

		
	}
}

$no = $argv[1];
generate($no);

?>