<?php
require_once '../vendor/autoload.php';
require_once 'database.php';

use \Firebase\JWT\JWT;

function verify($token,$privateKey)
{
	// $token = null;
	
	// if (isset($_GET['token'])) {$token = $_GET['token'];}

	if (!is_null($token) && $token != '') {


		// Get our server-side secret key from a secure location.
		$serverKey = $privateKey;

		try {
			$payload = JWT::decode($token, $serverKey, array('HS256'));
			$returnArray = array('status' => 'success','userId' => $payload->userId,'name' => $payload->name);
			if (isset($payload->exp)) {
				$returnArray['exp'] = date(DateTime::ISO8601, $payload->exp);;
			}
		}
		catch(Exception $e) {
			$returnArray = array('status' => 'failed','error' => $e->getMessage());
		}
	} 
	else {
		$returnArray = array('status' => 'failed','error' => 'You are not logged in with a valid token.');
	}

	return $returnArray;
}

function getIpRange($from , $to) {

	global $conn;
	if($from == '' || $to == '')
	{
		return array('status' => 'error', 'msg' => 'enter range');
	}

	$sql = "select * from router_prop where loopback BETWEEN INET_ATON('".cleanInp($from)."') AND INET_ATON('".cleanInp($to)."')";
	$result = mysqli_query($conn,$sql);
	if(isset($result) && !empty($result))
			$numrows= mysqli_num_rows($result);
	else
		$numrows = 0;
	
	$result_arr = array();
	for($i=0;$i<$numrows;$i++)
	{
		$result_arr[$i]['id'] = query_result($result,$i,'id');
		$result_arr[$i]['hostname'] = query_result($result,$i,'hostname');
		$result_arr[$i]['sap_id'] = query_result($result,$i,'sap_id');
		$result_arr[$i]['loopback'] = long2ip(query_result($result,$i,'loopback'));
		$result_arr[$i]['mac_addr'] = query_result($result,$i,'mac_addr');
		$result_arr[$i]['type'] = query_result($result,$i,'type');
	
	}

	return array('status' => 'success', 'msg' => '','data' => $result_arr);

}

function updateRouter($ip,$sapId,$hostname,$loopback,$mac,$type) {

	global $conn;
	if($ip == '')
	{
		return array('status' => 'error', 'msg' => 'enter ip');
	}

	$set = "";
	if($hostname != "")
		$set .= "hostname='".cleanInp($hostname)."',";
	if($loopback != "")
		$set .= "loopback=INET_ATON('".cleanInp($loopback)."'),";
	if($mac != "")
		$set .= "mac_addr='".cleanInp($mac)."',";
	if($sapId != "")
		$set .= "sap_id='".cleanInp($sapId)."',";
	if($type != "")
		$set .= "type='".cleanInp($type)."',";
	
	$set = rtrim($set,',');
	
	if($set == "")
		return array('status' => 'error', 'msg' => 'Enter Details');
	
	$checkExistsQuery = "SELECT	(SELECT COUNT(*) FROM router_prop WHERE hostname='".cleanInp($hostname)."' AND loopback!=INET_ATON('".cleanInp($ip)."')) as hostnameCnt, (SELECT COUNT(*) FROM router_prop where loopback=INET_ATON('".cleanInp($loopback)."') AND loopback!=INET_ATON('".cleanInp($ip)."')) as loopbackCnt,(SELECT COUNT(*) FROM router_prop WHERE mac_addr='".cleanInp($mac)."' AND loopback!=INET_ATON('".cleanInp($ip)."')) as macCount";
	$result = mysqli_query($conn,$checkExistsQuery);
	$hostnameCnt = query_result($result,0,'hostnameCnt');
	$loopbackCnt = query_result($result,0,'loopbackCnt');
	$macCount = query_result($result,0,'macCount');

	if($hostnameCnt > 0 || $loopbackCnt > 0 || $macCount > 0)
	{
		return array('status' => 'error', 'msg' => 'Duplicate record');
	}


	try{
		$sql = "update router_prop SET ".$set." WHERE loopback=INET_ATON('".cleanInp($ip)."')";
		$result = mysqli_query($conn,$sql);
		$returnArray = array('status' => 'success', 'msg' => 'updated');
	}
	catch(Exception $e) {
		$returnArray = array('status' => 'error','error' => $e->getMessage());
	}
	

	return $returnArray;

}

function createRouter($ip,$sapId,$hostname,$loopback,$mac,$type) {

	global $conn;
	

	$column = "";
	$values = "";
	if($hostname != "")
	{
		$column .= "hostname,";
		$values .= "'".cleanInp($hostname)."',";
	}
		
	if($loopback != "")
	{
		$column .= "loopback,";
		$values .= "INET_ATON('".cleanInp($loopback)."'),";
	}
	if($mac != "")
	{
		$column .= "mac_addr,";
		$values .= "'".cleanInp($mac)."',";
	}
	if($sapId != "")
	{
		$column .= "sap_id,";
		$values .= "'".cleanInp($sapId)."',";
	}
	if($type != "")
	{
		$column .= "type,";
		$values .= "'".cleanInp($type)."',";
	}
	
	$column = rtrim($column,',');
	$values = rtrim($values,',');
	
	if($column == "" || $values == "")
		return array('status' => 'error', 'msg' => 'Enter Details');
	
	$checkExistsQuery = "SELECT	(SELECT COUNT(*) FROM router_prop WHERE hostname='".cleanInp($hostname)."') as hostnameCnt, (SELECT COUNT(*) FROM router_prop where loopback=INET_ATON('".cleanInp($loopback)."')) as loopbackCnt,(SELECT COUNT(*) FROM router_prop WHERE mac_addr='".cleanInp($mac)."') as macCount";
	$result = mysqli_query($conn,$checkExistsQuery);
	$hostnameCnt = query_result($result,0,'hostnameCnt');
	$loopbackCnt = query_result($result,0,'loopbackCnt');
	$macCount = query_result($result,0,'macCount');

	if($hostnameCnt > 0 || $loopbackCnt > 0 || $macCount > 0)
	{
		return array('status' => 'error', 'msg' => 'Duplicate record');
	}


	try{
		$sql = "Insert INTO router_prop (".$column.") VALUES (".$values.")";
		//echo $sql;exit;
		$result = mysqli_query($conn,$sql);
		$returnArray = array('status' => 'success', 'msg' => 'created');
	}
	catch(Exception $e) {
		$returnArray = array('status' => 'error','error' => $e->getMessage());
	}
	

	return $returnArray;

}

function getRoutersOnTypeSap($sapId) {

	global $conn;
	if($sapId == '')
	{
		return array('status' => 'error', 'msg' => 'enter sapId');
	}

	try{
		$sql = "select * from router_prop where type=(select type from router_prop where sap_id='".cleanInp($sapId)."')";
		$result = mysqli_query($conn,$sql);
		$numrows = getRowCount($result);
		$result_arr = array();
		for($i=0;$i<$numrows;$i++)
		{
			$result_arr[$i]['id'] = query_result($result,$i,'id');
			$result_arr[$i]['hostname'] = query_result($result,$i,'hostname');
			$result_arr[$i]['sap_id'] = query_result($result,$i,'sap_id');
			$result_arr[$i]['loopback'] = long2ip(query_result($result,$i,'loopback'));
			$result_arr[$i]['mac_addr'] = query_result($result,$i,'mac_addr');
			$result_arr[$i]['type'] = query_result($result,$i,'type');
		
		}

		$returnArray = array('status' => 'success', 'msg' => '','data' => $result_arr);
	}
	catch(Exception $e) {
		$returnArray = array('status' => 'error','error' => $e->getMessage());
	}
	

	return $returnArray;

}

function deleteIp($ip) {

	global $conn;
	if($ip == '')
	{
		return array('status' => 'error', 'msg' => 'enter ip');
	}

	try{
		$sql = "delete from router_prop where loopback=INET_ATON('".cleanInp($ip)."')";
		$result = mysqli_query($conn,$sql);
		$returnArray = array('status' => 'success', 'msg' => 'deleted');
	}
	catch(Exception $e) {
		$returnArray = array('status' => 'error','error' => $e->getMessage());
	}
	

	return $returnArray;

}



?>