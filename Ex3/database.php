<?php

$dbconfig['db_host_name'] = 	'localhost';
$dbconfig['db_user_name'] = 	'root';
$dbconfig['db_password'] = 	'';
$dbconfig['db_name'] = 	'cisco';

$conn = mysqli_connect($dbconfig['db_host_name'], $dbconfig['db_user_name'], $dbconfig['db_password']);

if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}

mysqli_select_db($conn,$dbconfig['db_name']);


function query_result(&$result, $row, $col=0)
{		
	$numrows = getRowCount($result);
	if ($numrows && $row <= ($numrows-1) && $row >=0){
		mysqli_data_seek($result,$row);
		$resrow = (is_numeric($col)) ? mysqli_fetch_row($result) : mysqli_fetch_assoc($result);
		if (isset($resrow[$col])){
			return $resrow[$col];
		}
	}
	return false;
}

function getRowCount(&$result){
	//$this->println("ADODB getRowCount");
	if(isset($result) && !empty($result))
		$rows= mysqli_num_rows($result);				
	return $rows;			
}

function cleanInp($str)
{
	global $conn;
	$str = mysqli_real_escape_string($conn, $str);
	return $str;
}
?>