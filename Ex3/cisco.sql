-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 09, 2020 at 02:37 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cisco`
--

-- --------------------------------------------------------

--
-- Table structure for table `router_prop`
--

DROP TABLE IF EXISTS `router_prop`;
CREATE TABLE IF NOT EXISTS `router_prop` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sap_id` varchar(30) NOT NULL,
  `hostname` varchar(100) NOT NULL,
  `loopback` int(4) UNSIGNED NOT NULL,
  `mac_addr` varchar(100) NOT NULL,
  `type` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `HOSTNAME` (`hostname`),
  UNIQUE KEY `LOOPBACK` (`loopback`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `router_prop`
--

INSERT INTO `router_prop` (`id`, `sap_id`, `hostname`, `loopback`, `mac_addr`, `type`) VALUES
(1, '5338056660', 'www.np2yh8.com', 1552880176, '58:a7:36:98:20:fe', 'CSS'),
(2, '7556313028', 'www.gaELV1.com', 722073672, '64:f3:9b:9a:3d:99', 'AG1'),
(3, '9502699813', 'www.y7VXdz.com', 3164368103, '0d:99:b9:66:d0:f6', 'CSS'),
(4, '8417756099', 'www.4wQXls.com', 2222283883, 'd1:c7:cb:1c:a9:dc', 'AG1'),
(5, '7972150923', 'www.18dmLw.com', 1819570564, '65:ce:9f:73:fb:03', 'CSS'),
(6, '2271418696', 'www.wCR7n7.com', 734552534, 'd1:6c:88:a1:22:bb', 'AG1'),
(7, '2117462487', 'www.bKKGmA.com', 3221341359, '8a:17:27:66:e1:33', 'CSS'),
(8, '7249158063', 'www.xRsnDn.com', 1469109566, '51:6e:11:d5:c1:1d', 'AG1'),
(9, '1789714572', 'www.GOyyTo.com', 2358476014, '82:54:8b:fb:28:4e', 'CSS'),
(10, '2698857590', 'www.Y1EhfW.com', 103713127, '30:7a:fe:31:b4:6e', 'AG1');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
