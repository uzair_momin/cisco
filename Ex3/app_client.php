<?php
require '../vendor/autoload.php';
require_once 'database.php';
require_once 'helper.php';


use \Firebase\JWT\JWT;

$privateKey = '5f2b5cdbe5194f10b3241568fe4e2b24';

$requestMethod = $_SERVER['REQUEST_METHOD'];
$method = $_REQUEST['action'];
$method = $_REQUEST['request'];

if($requestMethod == 'POST' && $method == 'login')
{
	$username = '';
	$password = '';

	if (isset($_REQUEST['username'])) {$username = $_REQUEST['username'];}
	if (isset($_REQUEST['password'])) {$password = $_REQUEST['password'];}
	
	if (($username == 'john.doe') && ($password == 'foobar')) {


		/** 
		 * Create some payload data with user data we would normally retrieve from a
		 * database with users credentials. Then when the client sends back the token,
		 * this payload data is available for us to use to retrieve other data 
		 * if necessary.
		 */
		$userId = 'USER123456';

		/**
		 * Uncomment the following line and add an appropriate date to enable the 
		 * "not before" feature.
		 */
		// $nbf = strtotime('2021-01-01 00:00:01');

		/**
		 * Uncomment the following line and add an appropriate date and time to enable the 
		 * "expire" feature.
		 */
		// $exp = strtotime('2021-01-01 00:00:01');

		// Get our server-side secret key from a secure location.
		$serverKey = $privateKey;

		// create a token
		$payloadArray = array();
		$payloadArray['userId'] = $userId;
		$payloadArray['name'] = 'Jhon Doe';
		if (isset($nbf)) {$payloadArray['nbf'] = $nbf;}
		if (isset($exp)) {$payloadArray['exp'] = $exp;}
		$jwt = JWT::encode($payloadArray, $serverKey, 'HS256');

		// return to caller
		$returnArray = array('token' => $jwt);
		$jsonEncodedReturnArray = json_encode($returnArray, JSON_PRETTY_PRINT);
		echo $jsonEncodedReturnArray;

	} 
	else {
		$returnArray = array('error' => 'Invalid user ID or password.');
		$jsonEncodedReturnArray = json_encode($returnArray, JSON_PRETTY_PRINT);
		echo $jsonEncodedReturnArray;
	}
}
else if($requestMethod == 'GET' && $method == 'verify')
{
	$token = null;
	
	if (isset($_GET['token'])) {$token = $_GET['token'];}

	if (!is_null($token)) {


		// Get our server-side secret key from a secure location.
		$serverKey = $privateKey;

		try {
			$payload = JWT::decode($token, $serverKey, array('HS256'));
			$returnArray = array('userId' => $payload->userId,'name' => $payload->name);
			if (isset($payload->exp)) {
				$returnArray['exp'] = date(DateTime::ISO8601, $payload->exp);;
			}
		}
		catch(Exception $e) {
			$returnArray = array('error' => $e->getMessage());
		}
	} 
	else {
		$returnArray = array('error' => 'You are not logged in with a valid token.');
	}
	
	// return to caller
	$jsonEncodedReturnArray = json_encode($returnArray, JSON_PRETTY_PRINT);
	echo $jsonEncodedReturnArray;
}
else if($requestMethod == 'GET' && $method == 'getIpRange')
{
	$returnArray = verify($_GET['token'],$privateKey);
	
	if($returnArray['status'] != 'success')
	{
		$jsonEncodedReturnArray = json_encode($returnArray, JSON_PRETTY_PRINT);
		echo $jsonEncodedReturnArray;
		exit;
	}

	$returnArray = getIpRange($_GET['from'],$_GET['to']);
	
	
	// return to caller
	$jsonEncodedReturnArray = json_encode($returnArray, JSON_PRETTY_PRINT);
	echo $jsonEncodedReturnArray;
}
else if($requestMethod == 'POST' && $method == 'updateRouter')
{
	$returnArray = verify($_REQUEST['token'],$privateKey);
	
	if($returnArray['status'] != 'success')
	{
		$jsonEncodedReturnArray = json_encode($returnArray, JSON_PRETTY_PRINT);
		echo $jsonEncodedReturnArray;
		exit;
	}

	$returnArray = updateRouter($_REQUEST['ip'],$_REQUEST['sapId'],$_REQUEST['hostname'],$_REQUEST['loopback'],$_REQUEST['mac'],$_REQUEST['$type']);
	
	
	// return to caller
	$jsonEncodedReturnArray = json_encode($returnArray, JSON_PRETTY_PRINT);
	echo $jsonEncodedReturnArray;
}
else if($requestMethod == 'POST' && $method == 'createRouter')
{
	$returnArray = verify($_REQUEST['token'],$privateKey);
	
	if($returnArray['status'] != 'success')
	{
		$jsonEncodedReturnArray = json_encode($returnArray, JSON_PRETTY_PRINT);
		echo $jsonEncodedReturnArray;
		exit;
	}

	$returnArray = createRouter($_REQUEST['ip'],$_REQUEST['sapId'],$_REQUEST['hostname'],$_REQUEST['loopback'],$_REQUEST['mac'],$_REQUEST['type']);
	
	
	// return to caller
	$jsonEncodedReturnArray = json_encode($returnArray, JSON_PRETTY_PRINT);
	echo $jsonEncodedReturnArray;
}
else if($requestMethod == 'POST' && $method == 'deleteIp')
{
	$returnArray = verify($_REQUEST['token'],$privateKey);
	
	if($returnArray['status'] != 'success')
	{
		$jsonEncodedReturnArray = json_encode($returnArray, JSON_PRETTY_PRINT);
		echo $jsonEncodedReturnArray;
		exit;
	}

	$returnArray = deleteIp($_REQUEST['ip']);
	
	
	// return to caller
	$jsonEncodedReturnArray = json_encode($returnArray, JSON_PRETTY_PRINT);
	echo $jsonEncodedReturnArray;
}
else if($requestMethod == 'GET' && $method == 'getRoutersOnTypeSap')
{
	$returnArray = verify($_REQUEST['token'],$privateKey);
	
	if($returnArray['status'] != 'success')
	{
		$jsonEncodedReturnArray = json_encode($returnArray, JSON_PRETTY_PRINT);
		echo $jsonEncodedReturnArray;
		exit;
	}

	$returnArray = getRoutersOnTypeSap($_REQUEST['sapId']);
	
	
	// return to caller
	$jsonEncodedReturnArray = json_encode($returnArray, JSON_PRETTY_PRINT);
	echo $jsonEncodedReturnArray;
}



?>